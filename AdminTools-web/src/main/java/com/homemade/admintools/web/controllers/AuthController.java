package com.homemade.admintools.web.controllers;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.homemade.admintools.dao.auth.UserDAO;
import com.homemade.admintools.web.dto.UserTO;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping(value = "/auth")
@Slf4j
public class AuthController
{
    @Inject
    public AuthController(UserDAO userDAO)
    {
        this.userDAO = userDAO;
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout,
                              @RequestParam(value = "registered", required = false) String registered) 
    {
        log.debug("AuthController - error: {}, logout: {}", error, logout);
        
        ModelAndView model = new ModelAndView("login");        
        if (error != null) 
        {
            model.addObject("error", "Invalid username and/or password!");
        }
        if (logout != null) 
        {
            model.addObject("logout", "You've been logged out successfully.");
        }
        if (registered != null) 
        {
            model.addObject("registered", "You've successfully created an account!");
        }
        return model;
    }
    
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView showRegistrationPage() 
    {
        return new ModelAndView("register").addObject("newUser", new UserTO());
    }
    
    @RequestMapping(value = "/performRegistration", method = RequestMethod.POST)
    @Transactional
    public ModelAndView registerUserAndRedirectToLoginPage(@ModelAttribute("newUser") UserTO userTO) 
    {    	
        log.debug("userTO: {}", userTO);
        
        userDAO.create(userTO.getLogin(), userTO.getPassword());
        
        return new ModelAndView("login")
    		.addObject("registered", "You have successfully created an account!");
    }
    
    private final UserDAO userDAO;
}
