package com.homemade.admintools.web.dto;

import com.google.common.base.MoreObjects;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UserTO
{
    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("login", login)
                .toString();
    }
    
    private String login;
    private String password;
    private String passwordConfirm;
}
