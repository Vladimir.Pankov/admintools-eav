package com.homemade.admintools.web.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class ObjectTypeTO
{
    private String id;
    private String name;
    private String parentId;
    private boolean hasChildren;
    private List<ObjectTypeTO> childTypes;
}
