package com.homemade.admintools.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/manage")
public class GreetingsController
{       
    @RequestMapping(method = RequestMethod.GET)
    @Transactional
    public ModelAndView showHelloPage()
    {
        LOG.debug("GreetingsController logging!!!!!!");

        return new ModelAndView("greetings").addObject("otName", "ololo");
    }
    
    @RequestMapping(value = "test", method = RequestMethod.GET)
    @Transactional
    public ModelAndView doSmth()
    {
        return null;
    }

    private static final Logger LOG = LoggerFactory.getLogger(GreetingsController.class);
}