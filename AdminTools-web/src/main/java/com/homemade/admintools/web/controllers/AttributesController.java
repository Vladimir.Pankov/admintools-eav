package com.homemade.admintools.web.controllers;

import java.math.BigInteger;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.collect.Lists;

import com.homemade.admintools.common.transformers.Transformer;
import com.homemade.admintools.dao.eav.ObjectTypeDAO;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.metadata.ObjectType;
import com.homemade.admintools.web.dto.AttributeTO;

import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping(path = "/manage/attributes")
@Slf4j
public class AttributesController
{
    @Inject
    public AttributesController(ObjectTypeDAO objectTypeDAO,
                                Transformer<Attribute, AttributeTO> transformer)
    {
        this.objectTypeDAO = objectTypeDAO;
        this.transformer = transformer;
    }
    
    @RequestMapping(path = "/forObjectType/{id}", method = RequestMethod.GET)
    @Transactional
    public ModelAndView showAttributesOfObjectType(@PathVariable("id") BigInteger objectTypeId,
                                                   @RequestParam(name = "withInherited", required = false) 
                                                   boolean withInherited)
    {        
        log.debug("Showing attributes of Object Type with ID = {}, with inherited = {}",
                objectTypeId, withInherited);
        
        ObjectType objectType = objectTypeDAO.findById(objectTypeId);
        Set<Attribute> attributes = withInherited ? 
            objectType.getAttributesWithInherited() : objectType.getAttributes();
        List<AttributeTO> attributeTOs = Lists.newArrayList();
        for (Attribute attr : attributes)
        {
            attributeTOs.add(transformer.transform(attr));
        }        
        return new ModelAndView("attributesTable")
                .addObject("attributes", attributeTOs);
    }
    
    private final ObjectTypeDAO objectTypeDAO;
    private final Transformer<Attribute, AttributeTO> transformer;
}
