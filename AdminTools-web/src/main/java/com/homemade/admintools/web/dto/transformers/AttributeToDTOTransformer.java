package com.homemade.admintools.web.dto.transformers;

import javax.inject.Named;

import com.homemade.admintools.common.transformers.Transformer;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.web.dto.AttributeTO;

@Named("attributeToDTOTransformer")
public class AttributeToDTOTransformer implements Transformer<Attribute, AttributeTO>
{
    @Override
    public AttributeTO transform(Attribute attribute)
    {        
        AttributeTO attributeTO = new AttributeTO();
        attributeTO.setId(attribute.getId().toString());
        attributeTO.setName(attribute.getName());
        attributeTO.setMultiple(attribute.isMultiple());
        attributeTO.setCalculable(attribute.isCalculable());
        attributeTO.setType(attribute.getType().name());
        return attributeTO;
    }
}
