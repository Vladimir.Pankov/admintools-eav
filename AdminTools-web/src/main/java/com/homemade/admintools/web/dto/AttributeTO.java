package com.homemade.admintools.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter @Setter @ToString
public class AttributeTO
{
    private String id;
    private String name;
    private String type;
    private boolean multiple;
    private boolean calculable;
}
