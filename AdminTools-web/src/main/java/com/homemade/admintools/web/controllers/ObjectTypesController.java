package com.homemade.admintools.web.controllers;

import java.math.BigInteger;

import javax.inject.Inject;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import lombok.extern.slf4j.Slf4j;

import com.homemade.admintools.common.transformers.Transformer;
import com.homemade.admintools.dao.eav.ObjectTypeDAO;
import com.homemade.admintools.entities.eav.metadata.ObjectType;
import com.homemade.admintools.web.dto.ObjectTypeTO;

@Controller
@RequestMapping(path = "/manage/objectTypes")
@Slf4j
public class ObjectTypesController
{    
    @Inject
    public ObjectTypesController(ObjectTypeDAO objectTypeDAO, 
                                 Transformer<ObjectType, ObjectTypeTO> transformer)
    {
        this.objectTypeDAO = objectTypeDAO;
        this.transformer = transformer;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView initTree()
    {
        return new ModelAndView("manageMetadata")
                .addObject("newObjectType", new ObjectTypeTO());
    }  
    
    @RequestMapping(path = "/get/{id}", method = RequestMethod.GET, 
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Transactional
    public ObjectTypeTO getObjectType(@PathVariable("id") BigInteger id)
    {
        log.debug("Getting Object Type with ID = {}...", id);
        
        return transformer.transform(objectTypeDAO.findById(id));
    }
    
    @RequestMapping(path = "/create", method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Transactional
    public ObjectTypeTO createObjectType(@ModelAttribute("newObjectType") ObjectTypeTO objectTypeTO)
    {
        log.debug("Creating an Object Type by {}", objectTypeTO);
        
        ObjectType newObjectType = objectTypeDAO.create(objectTypeTO.getName(), 
                new BigInteger(objectTypeTO.getParentId()));
        return transformer.transform(newObjectType);
    }
    
    @RequestMapping(path = "/delete", method = RequestMethod.POST,
                    produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    @Transactional
    public ObjectTypeTO deleteObjectType(@RequestParam("objectTypeIdForRemoval") 
                                         BigInteger objectTypeId)
    {
        log.debug("Deleting an Object Type with ID = {}", objectTypeId);
        
        ObjectType objectType = objectTypeDAO.findById(objectTypeId);
        ObjectTypeTO objectTypeTO = transformer.transform(objectType);
        objectTypeDAO.delete(objectType);
        return objectTypeTO;
    }    
    
    private final ObjectTypeDAO objectTypeDAO;
    private final Transformer<ObjectType, ObjectTypeTO> transformer;
}
