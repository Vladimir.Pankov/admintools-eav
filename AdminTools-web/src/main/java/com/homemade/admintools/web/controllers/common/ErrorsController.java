package com.homemade.admintools.web.controllers.common;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.google.common.base.Throwables;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ErrorsController
{
    @RequestMapping(value = "errors", method = {
        RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE,
        RequestMethod.PATCH, RequestMethod.HEAD, RequestMethod.TRACE, RequestMethod.OPTIONS}
    )
    public ModelAndView handleErrors(HttpServletRequest httpRequest)
    {
        if (log.isDebugEnabled())
        {
        	log.debug("Error occurred, request parameters: {}, request attrs: {}", 
                httpRequest.getParameterMap(), Collections.list(httpRequest.getAttributeNames()));
        }
        
        ModelAndView modelAndView = new ModelAndView("errors");
        
        Exception exception = (Exception)httpRequest.getAttribute(HTTP_REQ_EXCEPTION_ATTR);
        if (exception != null)
        {
            modelAndView.addObject("stacktrace", Throwables.getStackTraceAsString(exception));
        }
        return modelAndView;
    }
    
    private static final String HTTP_REQ_EXCEPTION_ATTR = "javax.servlet.error.exception";
}
