package com.homemade.admintools.web.dto.transformers;

import java.util.List;

import javax.inject.Named;

import com.google.common.collect.Lists;

import com.homemade.admintools.common.transformers.Transformer;
import com.homemade.admintools.entities.eav.metadata.ObjectType;
import com.homemade.admintools.web.dto.ObjectTypeTO;

@Named("objectTypeToDTOTransformer")
public class ObjectTypeToDTOTransformer implements Transformer<ObjectType, ObjectTypeTO>
{
    @Override
    public ObjectTypeTO transform(ObjectType objectType)
    {
        ObjectTypeTO objTypeTO = createFrom(objectType);
        
        List<ObjectTypeTO> childTypes = Lists.newArrayList();
        for (ObjectType childType : objectType.getDescendants())
        {
            childTypes.add(createFrom(childType));
        }        
        objTypeTO.setChildTypes(childTypes);
        return objTypeTO;
    } 
    
    private ObjectTypeTO createFrom(ObjectType objectType)
    {
        ObjectTypeTO objTypeTO = new ObjectTypeTO();
        objTypeTO.setId(objectType.getId().toString());
        objTypeTO.setName(objectType.getName());
        ObjectType parentObjectType = objectType.getParentObjectType();
        if (parentObjectType != null)
        {
            objTypeTO.setParentId(parentObjectType.getId().toString());
        }
        objTypeTO.setHasChildren(!objectType.getDescendants().isEmpty());
        return objTypeTO;
    }
}
