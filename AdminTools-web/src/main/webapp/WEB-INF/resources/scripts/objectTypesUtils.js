var ObjectTypesTree = function(rootType, onObjectTypeSelectedCallback)
{
    var tree = this;    
    var treeView = ObjectTypesTree.get().treeview(
    {
        data: new ObjectTypesTreeDataBuilder().build(rootType),
        showTags: false,
        lazyLoad: ObjectTypesTree.loadChildTypesLazily,
        onNodeSelected: function(event, data)
        {
            tree.onObjectTypeSelected(event, data);
        }
    });
    this.treeViewData = ObjectTypesTree.getView();
    this.onObjectTypeSelectedCallback = onObjectTypeSelectedCallback;
};

ObjectTypesTree.get = function()
{
    return $("#objectTypesTree");
};

ObjectTypesTree.getView = function()
{
    return ObjectTypesTree.get().data('treeview');
};

ObjectTypesTree.loadChildTypesLazily = function(typeNode, addNodesCallback)
{
    console.log("Loading child types for node: ", typeNode);

    if (!typeNode.nodes || typeNode.nodes.length == 0)
    {
        $.get("objectTypes/get/" + typeNode.objectTypeId, function(objectType)
        {
            var newNodes = new ObjectTypesTreeDataBuilder()
                .buildAsNodes(objectType.childTypes);
            addNodesCallback(newNodes);
        });
    }
};

ObjectTypesTree.prototype.onObjectTypeSelected = function(event, data)
{
    console.log("Object Type selected...");
    
    var selectedObjectTypeNodes = this.treeViewData.getSelected();
    if (selectedObjectTypeNodes && selectedObjectTypeNodes.length > 0)
    {
        var objectTypeNode = selectedObjectTypeNodes[0];
        var id = objectTypeNode.objectTypeId;
        this.onObjectTypeSelectedCallback(objectTypeNode);
    }
};

ObjectTypesTree.toggleIds = function()
{
    console.log("toggleIds invoked...");
        
    var tree = ObjectTypesTree.get();
    var treeView = ObjectTypesTree.getView();    
    var treeViewOptions = treeView.options;
    treeViewOptions.showTags = !treeViewOptions.showTags;
    tree.treeview(treeViewOptions);
};

ObjectTypesTree.prototype.addObjectType = function(objectType)
{
    console.log("Adding Object Type with ID =", objectType.id, "to tree...");
    
    var parentNodes = this.treeViewData.findNodes(objectType.parentId, "objectTypeId");
    var parentNode = parentNodes[0];
    this.treeViewData.addNode(new ObjectTypesTreeDataBuilder().build(objectType), parentNode);
};

ObjectTypesTree.prototype.deleteObjectType = function(objectType)
{
    console.log("Removing Object Type with ID =", objectType.id, "from tree...");
    
    var objectTypeNodes = this.treeViewData.findNodes(objectType.id, "objectTypeId");    
    this.treeViewData.removeNode(objectTypeNodes);
};

var ObjectTypesTreeDataBuilder = function() { };

ObjectTypesTreeDataBuilder.prototype.build = function(rootType)
{
    return this.buildAsNodes([rootType]);
};

ObjectTypesTreeDataBuilder.prototype.buildAsSingleNode = function(rootType)
{
    console.log("Building Object Types tree starting from ", JSON.stringify(rootType));

    return this.buildRecursively(rootType);
};

ObjectTypesTreeDataBuilder.prototype.buildAsNodes = function(rootTypes)
{
    console.log("Building Object Types tree starting from ", JSON
            .stringify(rootTypes));

    var nodes = [];
    for (var i = 0; i < rootTypes.length; i++)
    {
        nodes.push(this.buildAsSingleNode(rootTypes[i]));
    }
    return nodes;
};

ObjectTypesTreeDataBuilder.prototype.buildRecursively = function(currentType)
{
    console.log("Adding Type ", JSON.stringify(currentType), " to tree");

    var treeNode = {};
    treeNode.text = currentType.name;
    treeNode.objectTypeId = currentType.id;
    treeNode.tags = ['ID = ' + currentType.id];
    treeNode.tagsClass = ['badge'];
    if (currentType.hasChildren)
    {
        treeNode.lazyLoad = true;
        treeNode.nodes = [];
        if (currentType.childTypes)
        {
            for (var i = 0; i < currentType.childTypes.length; i++)
            {
                treeNode.nodes.push(this.buildRecursively(currentType.childTypes[i]));
            }
        }
    }
    return treeNode;
};