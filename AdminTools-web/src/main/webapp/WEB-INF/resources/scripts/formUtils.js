var FormUtils = 
{
    submitAsync: function(formName, successCallback)
    {
        console.log("Submitting form", formName, "asynchronously...");
        
        var form = $('form[name="' + formName + '"]');        
        $.ajax({
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: successCallback
        });        
    }
};