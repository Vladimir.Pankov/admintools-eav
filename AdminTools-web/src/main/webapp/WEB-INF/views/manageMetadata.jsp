<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="staticResources.jsp" />
        <link type="text/css" rel="stylesheet"
              href="<c:url value="/resources/patternfly-bootstrap-treeview/bootstrap-treeview.css" />">
        <script src="<c:url value="/resources/patternfly-bootstrap-treeview/bootstrap-treeview.js" />"></script>
        <script src="<c:url value="/resources/scripts/objectTypesUtils.js" />"></script>
        <script src="<c:url value="/resources/scripts/formUtils.js" />"></script>
        <title>AdminTools - Manage Metadata</title>
    </head>
    <body>
        <script type="text/javascript">
            var objectTypesTree;
            $(document).ready(function()
            {
                $.get("objectTypes/get/1", function(rootType)
                {
                    console.log("Initializing Tree View with data = ", rootType);
    
                    objectTypesTree = new ObjectTypesTree(rootType, function(objectTypeNode)
                    {
                        var objectTypeId = objectTypeNode.objectTypeId;
                        $("#parentId").val(objectTypeId);
                        $("#objectTypeIdForRemoval").val(objectTypeId);
                        $.get("attributes/forObjectType/" + objectTypeId, function(content)
                        {
                            console.log("Drawing new attributes table: ", content);
                            
                            $("#attributesTable").html(content);
                        });
                    });
                })
            });
        </script>
        <div class="container">
            <jsp:include page="header.jsp" />
            <div id="createObjectTypeModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">New Object Type</h4>
                        </div>
                        <c:url var="createObjectTypeUrl" value="/manage/objectTypes/create" />
                        <div class="modal-body">
                            <sf:form method="POST" action="${createObjectTypeUrl}" name="createObjectTypeForm" 
                                     class="modalForm form-horizontal"
                                     modelAttribute="newObjectType">
                                <div class="form-group">
                                    <label for="name" class="col-sm-2 control-label">Name</label>
                                    <div class="col-sm-10">
                                        <sf:input type="text" class="form-control" id="name" 
                                                          path="name" placeholder="Object Type Name"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="parentId" class="col-sm-2 control-label">Parent</label>
                                    <div class="col-sm-10">
                                        <sf:input type="text" class="form-control" id="parentId"
                                                          path="parentId" placeholder="Parent" value="1"/>
                                    </div>
                                </div>
                                <jsp:include page="csrfToken.jsp" />                                
                            </sf:form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <a onclick="submitCreateObjectTypeForm();return false;" class="btn btn-primary" data-dismiss="modal">Create</a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="deleteObjectTypeModal" class="modal fade" tabindex="-1" role="dialog">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <h4 class="modal-title">Deleting Object Type</h4>
                        </div>
                        <c:url var="deleteObjectTypeUrl" value="/manage/objectTypes/delete" />
                        <div class="modal-body">
                            <p>Are you sure you want to delete the selected Object Type?</p>
                            <form method="POST" action="${deleteObjectTypeUrl}" name="deleteObjectTypeForm" 
                                     class="modalForm form-horizontal">
                                <input type="hidden" id="objectTypeIdForRemoval" name="objectTypeIdForRemoval" value=""/>
                                <jsp:include page="csrfToken.jsp" />                                
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                            <a onclick="submitDeleteObjectTypeForm();return false;" class="btn btn-primary" data-dismiss="modal">Yes</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div>
                        <!-- Tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#objectTypesTabPanel" aria-controls="objectTypesTabPanel" role="tab" data-toggle="tab">Object Types</a>
                            </li>
                            <li role="presentation">
                                <a href="#attrTypeDefsTabPanel" aria-controls="attrTypeDefsTabPanel" role="tab" data-toggle="tab">Attribute Type Definitions</a>
                            </li>
                        </ul>
                        <!-- Tab content -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="objectTypesTabPanel">
                                <br/>
                                <p>
                                    <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                            data-target="#createObjectTypeModal">
                                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create
                                    </button>
                                    <button type="button" class="btn btn-primary btn-xs">
                                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                                    </button>
                                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                            data-target="#deleteObjectTypeModal">
                                        <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Delete
                                    </button>
                                    <label class="checkbox-inline"> <input id="showIdsCheckbox" type="checkbox" onclick="ObjectTypesTree.toggleIds()">Show IDs
                                    </label>
                                </p>
                                <div id="objectTypesTree"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="attrTypeDefsTabPanel">...</div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#attributesPanel" aria-controls="attributesPanel" 
                               role="tab" data-toggle="tab">Attributes</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="attributesPanel">
                            <br/>
                            <p>
                                <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                        data-target="#createAttributeModal">
                                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Create
                                </button>
                                <button type="button" class="btn btn-primary btn-xs">
                                    <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Edit
                                </button>
                                <button type="button" class="btn btn-danger btn-xs" data-toggle="modal"
                                        data-target="#deleteAttributeModal">
                                    <span class="glyphicon glyphicon-minus" aria-hidden="true"></span> Delete
                                </button>
                            </p>
                            <div id="attributesTable"></div>
                        </div>                        
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
           function submitCreateObjectTypeForm()
           {              
               FormUtils.submitAsync("createObjectTypeForm", function(objectType)
               {                   
                   objectTypesTree.addObjectType(objectType);
               });
           }
           
           function submitDeleteObjectTypeForm()
           {
               FormUtils.submitAsync("deleteObjectTypeForm", function(objectType)
               {                   
                   objectTypesTree.deleteObjectType(objectType);
               });
           }
        </script>
    </body>
</html>