<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<table class="table table-striped">
    <thead>
        <tr>
            <th>#</th>
            <th>ID</th>
            <th>Name</th>
            <th>Type</th>
            <th>Is Multiple</th>
            <th>Is Calculable</th>
        </tr>
    </thead>
    <tbody>
        <c:forEach var="attr" items="${attributes}" varStatus="loopStatus">
            <tr>
                <td>${loopStatus.count}</td>
                <td><c:out value="${attr.id}" /></td>
                <td><c:out value="${attr.name}" /></td>
                <td><c:out value="${attr.type}" /></td>
                <td><c:out value="${attr.multiple}"/></td>
                <td><c:out value="${attr.calculable}"/></td>
            </tr>
        </c:forEach>
    </tbody>
</table>