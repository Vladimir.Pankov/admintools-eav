<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="sf" %>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="staticResources.jsp" />
        <title>AdminTools - Register</title>
    </head>
    <body>
        <div class="container">
            <jsp:include page="header.jsp" />
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="col-md-offset-5">
                        <h3>Register</h3>
                    </div>
                    <c:url var="registerUserUrl" value="/auth/performRegistration" />
                    <sf:form method="POST" action="${registerUserUrl}" modelAttribute="newUser" class="form-horizontal">
                        <fieldset>
                        	<div class="form-group">
                                <label for="loginInput" class="col-sm-2 control-label">Login</label>
                                <div class="col-sm-10">
                                    <sf:input type="text" class="form-control" name="login" 
                                              path="login" placeholder="Login" required="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="passwordInput" class="col-sm-2 control-label">Password</label>
                                <div class="col-sm-10">
                                    <sf:input type="password" class="form-control" name="password" 
                                              path="password" placeholder="Password" required="true" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="passwordConfirmInput" class="col-sm-2 control-label">Password Confirmation</label>
                                <div class="col-sm-10">
                                    <sf:input type="password" class="form-control" name="passwordConfirm" 
                                              path="passwordConfirm" placeholder="Password Confirmation" required="true" />
                                </div>
                            </div>
                        </fieldset>
                        <jsp:include page="csrfToken.jsp" />
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <a href="<c:url value='/auth/login' />" class="btn btn-default">Cancel</a>
                                <button type="submit" class="btn btn-primary">Create Account</button>
                            </div>
                        </div>                       
                    </sf:form>
                </div>
            </div>
        </div>
    </body>
</html>