<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="staticResources.jsp" />
        <title>AdminTools - Login</title>
    </head>
    <body>
        <div class="container">
            <jsp:include page="header.jsp" />
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="col-md-offset-5">
                        <h3>Sign In</h3>
                    </div>
                    <c:if test="${not empty error}">
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>${error}
                        </div>
                    </c:if>
                    <c:if test="${not empty logout}">
                        <div class="alert alert-info alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>${logout}
                        </div>
                    </c:if>
                    <c:if test="${not empty registered}">
                        <div class="alert alert-success alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>${registered}
                        </div>
                    </c:if>
                    <form class="form-horizontal" action="performLogin" method="POST">
                        <div class="form-group">
                            <label for="loginInput" class="col-sm-2 control-label">Login</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="login" placeholder="Login">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="passwordInput" class="col-sm-2 control-label">Password</label>
                            <div class="col-sm-10">
                                <input type="password" class="form-control" name="password"
                                    placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <div class="checkbox">
                                    <label> <input type="checkbox"> Remember me
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Sign in</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="text-center">
                                Don't have an account yet? <a href="register">Create one.</a>
                            </div>
                        </div>
                        <jsp:include page="csrfToken.jsp" />
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>