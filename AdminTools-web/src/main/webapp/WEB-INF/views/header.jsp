<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script src="<c:url value="/resources/scripts/security/logoutUtils.js" />"></script>
<nav class="navbar navbar-default">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                class="icon-bar"></span> <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">EAV AdminTools</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <sec:authorize access="isAnonymous()">
                <li><a href="<c:url value='/auth/login' />">Log in</a></li>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                <li><a href="<c:url value='/manage/objectTypes' />">Object Types</a></li>
            </sec:authorize>
        </ul>
        <sec:authorize access="isAuthenticated()">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:performLogout()">Log out</a></li>
            </ul>
            <form id="logoutForm" action="<c:url value='/auth/performLogout' />" method="POST">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
            </form>
        </sec:authorize>
    </div>
</nav>