<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <jsp:include page="staticResources.jsp" />
        <title>AdminTools - Error occurred</title>
    </head>
    <body>
        <div class="container">
            <jsp:include page="header.jsp" />
            Error occurred:<br />
            <pre>${stacktrace}</pre>
        </div>
    </body>
</html>