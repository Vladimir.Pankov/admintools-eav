package com.homemade.admintools.spring;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import com.homemade.admintools.dao.eav.AttributeDAO;
import com.homemade.admintools.dao.eav.ObjectTypeDAO;
import com.homemade.admintools.dao.eav.RawObjectParameterValueDAO;

@Named
public class BeansHolder
{
    public static BeansHolder getInstance()
    {
        return instance;
    }
    
    public DataSource getDataSource()
    {
        return dataSource;
    }
    
    public EntityManagerFactory getHibernate()
    {
        return entityManagerFactory;
    }
    
    public AttributeDAO getAttributeDAO()
    {
        return attributeDAO;
    }
   
    public ObjectTypeDAO getObjectTypeDAO()
    {
        return objectTypeDAO;
    }
    
    public RawObjectParameterValueDAO getRawObjectParameterValueDAO()
    {
        return rawObjectParameterValueDAO;
    }
    
    @PostConstruct
    private void initialize()
    {
        instance = this;
    }
    
    @PreDestroy
    private void freeResources()
    {
        instance = null;
    }
    
    private BeansHolder() { }
    
    @Inject
    private DataSource dataSource;
    
    @Inject
    private EntityManagerFactory entityManagerFactory;
    
    @Inject
    private RawObjectParameterValueDAO rawObjectParameterValueDAO;
    
    @Inject
    private AttributeDAO attributeDAO;
    
    @Inject
    private ObjectTypeDAO objectTypeDAO;
    
    private static volatile BeansHolder instance;    
}
