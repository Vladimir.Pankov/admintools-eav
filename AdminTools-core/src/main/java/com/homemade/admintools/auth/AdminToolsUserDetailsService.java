package com.homemade.admintools.auth;

import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

import com.homemade.admintools.dao.auth.UserDAO;
import com.homemade.admintools.entities.core.AdminToolsUser;

import lombok.extern.slf4j.Slf4j;

@Named("adminToolsUserDetailsService")
@Slf4j
public class AdminToolsUserDetailsService implements UserDetailsService
{
    @Inject
    public AdminToolsUserDetailsService(UserDAO userDAO)
    {
        this.userDAO = userDAO;
    }
    
    @Override
    @Transactional
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException
    {
        log.debug("Finding User by name = {}", userName);
        
        AdminToolsUser user = userDAO.findByUserName(userName);
        
        log.debug("Found user: {}", user);
        
        if (user == null)
        {
            throw new UsernameNotFoundException("User with name = " + userName + " not found");
        }        
        return new User(user.getUserName(), user.getPassword(), user.getAuthorities());
    }

    private final UserDAO userDAO;
}
