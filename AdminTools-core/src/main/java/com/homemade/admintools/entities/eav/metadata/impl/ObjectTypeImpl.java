package com.homemade.admintools.entities.eav.metadata.impl;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;

import com.homemade.admintools.entities.eav.BaseEntityImpl;
import com.homemade.admintools.entities.eav.metadata.AttrToObjectTypeBinding;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.metadata.BindType;
import com.homemade.admintools.entities.eav.metadata.ObjectType;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Entity
@Table(name = "OBJECT_TYPES")
@AttributeOverride(name = "id", column = @Column(name = "OBJECT_TYPE_ID"))
@Slf4j
public class ObjectTypeImpl extends BaseEntityImpl implements ObjectType
{        
    @Override
    public Set<Attribute> getAttributesWithInherited()
    {
        if (attributesWithInherited == null)
        {
            attributesWithInherited = obtainInheritedAttrs();
        }
        return attributesWithInherited;
    }
    
    @Override
    public String toString()
    {
        ObjectType parentOt = getParentObjectType();
        return MoreObjects.toStringHelper(this)
                .add("id", getId())
                .add("name", getName())
                .add("parentObjectType", parentOt != null ? parentOt.getId() : null)
            .toString();
    }
    
    @PostLoad
    private void prepareObjectType()
    {
        log.debug("Performing post-loading actions for {}...", this);
           
        collectAttributes();
                
        log.debug("Collected attributes: {}", attributes);
    }
    
    private void collectAttributes()
    {
        for (AttrToObjectTypeBinding binding : attrBindings)
        {
            if (binding.getBindType() == BindType.BOUND)
            {
                attributes.add(binding.getAttribute());
            }
        }
    }
    
    private Set<Attribute> obtainInheritedAttrs()
    {
        Set<Attribute> allAttrs = Sets.newHashSet();
        allAttrs.addAll(getAttributes());
        ObjectType parentOT = getParentObjectType();
        while (parentOT != null)
        {
            allAttrs.addAll(parentOT.getAttributes());
            parentOT = parentOT.getParentObjectType();
        }
        return allAttrs;
    }
    
    @ManyToOne(targetEntity = ObjectTypeImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID")
    @Getter
    @Setter
    private ObjectType parentObjectType;
    
    @OneToMany(targetEntity = ObjectTypeImpl.class, 
               mappedBy = "parentObjectType", 
               fetch = FetchType.LAZY)
    @Getter
    private Set<ObjectType> descendants = Sets.newHashSet(); 
    
    @OneToMany(targetEntity = AttrToObjectTypeBindingImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "OBJECT_TYPE_ID")
    private Set<AttrToObjectTypeBinding> attrBindings = Sets.newHashSet();
    
    @Transient
    @Getter
    private Set<Attribute> attributes = Sets.newHashSet();
    
    @Transient
    private Set<Attribute> attributesWithInherited;
    
    private static final long serialVersionUID = -1698300844163995658L;
}
