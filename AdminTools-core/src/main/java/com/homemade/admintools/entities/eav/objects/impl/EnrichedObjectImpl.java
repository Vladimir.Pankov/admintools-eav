package com.homemade.admintools.entities.eav.objects.impl;

import java.math.BigInteger;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;

import com.homemade.admintools.entities.eav.objects.EnrichedObject;
import com.homemade.admintools.entities.eav.objects.RawObject;
import com.homemade.admintools.entities.eav.params.Parameter;
import com.homemade.admintools.entities.eav.params.ParameterImpl;
import com.homemade.admintools.entities.eav.metadata.ObjectType;

public class EnrichedObjectImpl implements EnrichedObject
{
    public EnrichedObjectImpl(RawObject objectEntity)
    {
        this.rawObject = objectEntity;
        this.boundAttrIds = objectEntity.getObjectType().getAttributesWithInherited()
                .stream().map(attr -> attr.getId()).collect(Collectors.toSet());
    }
    
    @Override
    public BigInteger getId()
    {
        return rawObject.getId();
    }

    @Override
    public String getName()
    {
        return rawObject.getName();
    }

    @Override
    public void setName(String name)
    {
        rawObject.setName(name);
    }
    
    @Override
    public ObjectType getObjectType()
    {
        return rawObject.getObjectType();
    }

    @Override
    public BigInteger getObjectTypeId()
    {
        return rawObject.getObjectTypeId();
    }
    
    @Override
    public EnrichedObject getParent()
    {
        return new EnrichedObjectImpl(rawObject.getParent());
    }

    @Override
    public BigInteger getParentId()
    {
        return rawObject.getParentId();
    }
    
    @Override
    public void setParentId(BigInteger parentId)
    {
        rawObject.setParentId(parentId);
    }

    @Override
    public String getDescription()
    {
        return rawObject.getDescription();
    }

    @Override
    public void setDescription(String description)
    {
        rawObject.setDescription(description);
    }

    @Override
    public Parameter getParameter(BigInteger attrId)
    {
        Preconditions.checkArgument(boundAttrIds.contains(attrId), 
                "Attribute with ID = " + attrId + " is not bound to current object");
        
        Parameter parameter = attrIdToParam.get(attrId);
        if (parameter == null)
        {
            parameter = new ParameterImpl(attrId, rawObject);
            attrIdToParam.put(attrId, parameter);
        }
        return parameter;
    }
    
    @Override
    public RawObject getRawObject()
    {
        return rawObject;
    } 
    
    @Override
    public String toString()
    {
    	return MoreObjects.toStringHelper(this)
	        .add("rawObject", rawObject)
	        .add("boundAttrIds", boundAttrIds)
	        .add("attrIdToParam", attrIdToParam)
	        .toString();
    }
    
    private final RawObject rawObject;
    private final Set<BigInteger> boundAttrIds;
    private final Map<BigInteger, Parameter> attrIdToParam = Maps.newHashMap();
}
