package com.homemade.admintools.entities.eav.metadata.impl;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.homemade.admintools.entities.eav.BaseEntityImpl;
import com.homemade.admintools.entities.eav.metadata.AttrTypeDef;
import com.homemade.admintools.entities.eav.metadata.ListValue;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "LIST_VALUES")
@AttributeOverride(name = "id", column = @Column(name = "LIST_VALUE_ID"))
@Getter
@Setter
public class ListValueImpl extends BaseEntityImpl implements ListValue
{      
    @Column(name = "ADDITIONAL")
    private String additional;
    
    @ManyToOne(targetEntity = AttrTypeDefImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "ATTR_TYPE_DEF_ID")
    private AttrTypeDef attrTypeDef;

    private static final long serialVersionUID = 7490947611371443840L;
}
