package com.homemade.admintools.entities.eav.metadata;

import java.math.BigInteger;

public interface AttrToObjectTypeBinding
{
    BigInteger getAttrId();
    
    BigInteger getObjectTypeId();
    
    Attribute getAttribute();
    
    BindType getBindType();
    
    void setBindType(BindType bindType);
}
