package com.homemade.admintools.entities.core.queries;

import javax.persistence.MappedSuperclass;
import javax.persistence.NamedQuery;

import javax.persistence.NamedQueries;

@MappedSuperclass
@NamedQueries(value = {
	@NamedQuery
	(
		name = "AdminToolsUser.findByUserName",
		query = "select distinct rawObject " +
		        "from RawObjectImpl as rawObject " +
		        "where rawObject.name = :name " +
		        "      and rawObject.objectTypeId = 9000149295739835221"
	)
})
public class UserQueries 
{ 	
	private UserQueries() { }
}
