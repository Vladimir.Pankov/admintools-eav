package com.homemade.admintools.entities.eav.metadata;

import java.math.BigInteger;
import java.util.Date;

import com.google.common.base.Preconditions;

public enum AttrType
{
    TEXT(1, String.class),
    NUMBER(2, Number.class), 
    DATE(3, Date.class), 
    LIST(4, ListValue.class), 
    REFERENCE(5, BigInteger.class);
    
    public int getId()
    {
        return id;
    }
    
    public Class<?> getSupportedValueType()
    {
        return supportedValueType;
    }
    
    public void validateValue(Object value)
    {
        if (value != null)
        {
            validateValueType(value.getClass());
        }
    }
    
    public void validateValueType(Class<?> valueType)
    {
        Preconditions.checkArgument(supportedValueType.isAssignableFrom(valueType),
                String.format(UNSUPPORTED_VALUE_TYPE_MSG, valueType, this, supportedValueType));
    }
    
    public static AttrType withId(int id)
    {
        for (AttrType attrType : AttrType.values())
        {
            if (attrType.getId() == id)
            {
                return attrType;
            }
        }
        return null;
    }
    
    AttrType(int id, Class<?> supportedValueType)
    {
        this.id = id;
        this.supportedValueType = supportedValueType;
    }
    
    private int id;
    private Class<?> supportedValueType;
    
    private static final String UNSUPPORTED_VALUE_TYPE_MSG = 
        "Value of type '%s' is not supported by the attribute of type '%s', supported type: %s";
}
