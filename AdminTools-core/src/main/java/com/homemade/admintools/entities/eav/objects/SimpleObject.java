package com.homemade.admintools.entities.eav.objects;

import java.math.BigInteger;

import com.homemade.admintools.entities.eav.BaseEntity;
import com.homemade.admintools.entities.eav.metadata.ObjectType;

public interface SimpleObject extends BaseEntity
{
    BigInteger getObjectTypeId();
    
    ObjectType getObjectType();
 
    BigInteger getParentId();
    
    void setParentId(BigInteger parentId);
    
    String getDescription();
    
    void setDescription(String description);
}
