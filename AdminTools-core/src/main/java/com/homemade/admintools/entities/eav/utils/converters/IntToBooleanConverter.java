package com.homemade.admintools.entities.eav.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class IntToBooleanConverter implements AttributeConverter<Boolean, Integer>
{
    @Override
    public Integer convertToDatabaseColumn(Boolean flag)
    {
        return flag ? 1 : 0;
    }

    @Override
    public Boolean convertToEntityAttribute(Integer integer)
    {
        return integer != 0;
    }
}