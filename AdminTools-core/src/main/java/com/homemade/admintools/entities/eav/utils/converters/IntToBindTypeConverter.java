package com.homemade.admintools.entities.eav.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.homemade.admintools.entities.eav.metadata.BindType;

@Converter
public class IntToBindTypeConverter implements AttributeConverter<BindType, Integer>
{
    @Override
    public Integer convertToDatabaseColumn(BindType bindType)
    {
        return bindType.getId();
    }

    @Override
    public BindType convertToEntityAttribute(Integer dbBindTypeId)
    {
        return BindType.withId(dbBindTypeId);
    }
}
