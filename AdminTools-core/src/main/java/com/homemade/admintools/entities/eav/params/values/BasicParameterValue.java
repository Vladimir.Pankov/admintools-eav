package com.homemade.admintools.entities.eav.params.values;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.Transient;

import com.google.common.base.Objects;
import com.google.common.base.MoreObjects;
import com.google.common.base.Preconditions;
import com.google.common.base.MoreObjects.ToStringHelper;

import com.homemade.admintools.entities.eav.params.ModifiableParameterValue;
import com.homemade.admintools.entities.eav.params.keys.ParameterKey;
import com.homemade.admintools.dao.eav.AttributeDAO;
import com.homemade.admintools.entities.eav.metadata.AttrType;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.metadata.ListValue;
import com.homemade.admintools.entities.eav.metadata.impl.AttributeImpl;
import com.homemade.admintools.spring.BeansHolder;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public abstract class BasicParameterValue implements ModifiableParameterValue
{    
    @Override
    public BigInteger getAttrId()
    {
        return key.getAttrId();
    }
        
    @Override
    public BigInteger getObjectId()
    {
        return key.getObjectId();
    }
    
    @Override
    public <T> T getValue(Class<T> valueType)
    {
        return getValueInternal(valueType);
    }
    
    @Override
    public void setValue(Object value)
    {
        Preconditions.checkNotNull(value, "Setting null is not allowed");
        
        AttrType attrType = attribute.getType();
        attrType.validateValueType(value.getClass());
        switch (attrType)
        {
            case DATE:
                setDateValue((Date)value);
                break;
            case LIST:
                setListValue((ListValue)value);
                break;
            case NUMBER:
            case TEXT:
                setStringValue(value.toString());
                break;
            case REFERENCE:
                setReference((BigInteger)value);
                break;
            default:
                throw new IllegalStateException();                
        }
    }
    
    @Override
    public int hashCode()
    {
        return Objects.hashCode(getKey());
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        BasicParameterValue other = (BasicParameterValue)obj;
        return Objects.equal(getKey(), other.getKey());
    }

    @Override
    public String toString()
    {
        ToStringHelper toStringHelper = MoreObjects.toStringHelper(this)
            .add("attrId", getAttrId())
            .add("objectId", getObjectId());
        fillToStringHelper(toStringHelper);
        return toStringHelper.toString();
    }
    
    protected void fillToStringHelper(ToStringHelper toStringHelper) { }
    
    @SuppressWarnings("unchecked")
    private <T> T getValueInternal(Class<T> valueType)
    {
        AttrType attrType = attribute.getType();
        attrType.validateValueType(valueType);
        switch (attrType)
        {
            case DATE:
                return (T)getDateValue();                
            case LIST:
                return (T)getListValue();
            case NUMBER:
                return (T)new BigInteger(getStringValue());                
            case REFERENCE:
                return (T)getReference();
            case TEXT:
                return (T)getStringValue();
            default:
                throw new IllegalStateException();                
        }
    }
    
    @PrePersist
    private void prePersist()
    {        
        if (attribute == null)
        {
            attribute = attributeDAO.findById(getAttrId());
        }
    }
    
    @Id
    @Getter(AccessLevel.PROTECTED)
    @Setter(AccessLevel.PROTECTED)
    private ParameterKey key;
    
    @ManyToOne(targetEntity = AttributeImpl.class)
    @JoinColumn(name = "ATTR_ID", insertable = false, updatable = false)
    @Getter(AccessLevel.PROTECTED)
    private Attribute attribute;
    
    @Transient
    private final AttributeDAO attributeDAO = BeansHolder.getInstance().getAttributeDAO();
}
