package com.homemade.admintools.entities.eav.objects.impl;

import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.AttributeOverride;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.google.common.base.MoreObjects.ToStringHelper;
import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import com.homemade.admintools.entities.eav.BaseEntityImpl;
import com.homemade.admintools.entities.eav.objects.RawObject;
import com.homemade.admintools.entities.eav.params.ModifiableParameterValue;
import com.homemade.admintools.entities.eav.params.values.ReferenceParameterValue;
import com.homemade.admintools.entities.eav.params.values.SimpleParameterValue;
import com.homemade.admintools.spring.BeansHolder;
import com.homemade.admintools.entities.eav.metadata.ObjectType;
import com.homemade.admintools.entities.eav.metadata.impl.ObjectTypeImpl;

@Entity
@Table(name = "OBJECTS")
@AttributeOverride(name = "id", column = @Column(name = "OBJECT_ID"))
@Slf4j
public class RawObjectImpl extends BaseEntityImpl implements RawObject
{   
    public RawObjectImpl() { }
    
    public RawObjectImpl(BigInteger objectTypeId)
    {
        this.objectTypeId = objectTypeId;
    }
    
    @Override
    public List<? extends ModifiableParameterValue> getSimpleParamValues()
    {
        return parameterValues;
    }
    
    @Override
    public List<? extends ModifiableParameterValue> getRefParamValues()
    {
        return referenceValues;
    }
    
    @Override
    public List<ModifiableParameterValue> getParamValues(BigInteger attrId)
    {        
        return Stream.concat(parameterValues.stream(), referenceValues.stream())
                .filter(paramValue -> Objects.equals(attrId, paramValue.getAttrId()))
                .collect(Collectors.toList());
    }
    
    @Override
    protected void fillToStringHelper(ToStringHelper toStringHelper) 
    {
        super.fillToStringHelper(toStringHelper);
        toStringHelper
            .add("objectTypeId", objectType.getId())
            .add("description", description);
    }
    
    @PrePersist
    private void init()
    {
        log.debug("Initializing RawObject...");
        
        if (objectType == null)
        {
            objectType = BeansHolder.getInstance().getObjectTypeDAO().findById(objectTypeId);
        }
    }
    
    @Column(name = "DESCRIPTION")
    @Getter
    @Setter
    private String description;
    
    @Column(name = "OBJECT_TYPE_ID")
    @Getter    
    private BigInteger objectTypeId;
    
    @Column(name = "PARENT_ID")
    @Getter
    @Setter
    private BigInteger parentId;
    
    @ManyToOne(targetEntity = ObjectTypeImpl.class)
    @JoinColumn(name = "OBJECT_TYPE_ID", updatable = false, insertable = false)
    @Getter
    private ObjectType objectType;

    @ManyToOne(targetEntity = RawObjectImpl.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_ID", updatable = false, insertable = false)
    @Getter
    private RawObject parent;
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "OBJECT_ID")    
    private List<SimpleParameterValue> parameterValues = Lists.newArrayList();
    
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "OBJECT_ID")
    private List<ReferenceParameterValue> referenceValues = Lists.newArrayList();
    
    private static final long serialVersionUID = -6731290664489897506L;
}
