package com.homemade.admintools.entities.eav.utils.idgenerators;

import java.io.Serializable;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.spi.JdbcCoordinator;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

public class GlobalIdGenerator implements IdentifierGenerator
{
    @Override
    public Serializable generate(SharedSessionContractImplementor session, Object object) 
        throws HibernateException
    {
        LOG.debug("Generating id for object {}", object);
        
        JdbcCoordinator jdbcCoordinator = session.getJdbcCoordinator();
        try 
        {
            PreparedStatement statement = jdbcCoordinator.getStatementPreparer()
                .prepareStatement(GET_ID_SQL);
            try 
            {
                ResultSet resultSet = jdbcCoordinator.getResultSetReturn().extract(statement);
                try 
                {
                    resultSet.next();
                    BigInteger generatedId = resultSet.getBigDecimal(1).toBigInteger();
                    
                    LOG.debug("Generated id: {}", generatedId);
                    
                    return generatedId;
                }
                finally 
                {
                    jdbcCoordinator.getLogicalConnection().getResourceRegistry()
                        .release(resultSet, statement);
                }
            }
            finally 
            {
                jdbcCoordinator.getLogicalConnection().getResourceRegistry().release(statement);
                jdbcCoordinator.afterStatementExecution();
            }
        }
        catch (SQLException sqlException) 
        {
            LOG.error("ID not generated", sqlException);
            
            throw session.getJdbcServices().getSqlExceptionHelper().convert(
                sqlException,
                "could not get next sequence value",
                GET_ID_SQL
            );
        }
    }
    
    private static final String GET_ID_SQL = "select get_id()";
    
    private static final Logger LOG = LoggerFactory.getLogger(GlobalIdGenerator.class);
}
