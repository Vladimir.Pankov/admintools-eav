package com.homemade.admintools.entities.eav.utils.converters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.homemade.admintools.entities.eav.metadata.AttrType;

@Converter
public class IntToAttrTypeConverter implements AttributeConverter<AttrType, Integer>
{
    @Override
    public Integer convertToDatabaseColumn(AttrType attrType)
    {
        return attrType.getId();
    }

    @Override
    public AttrType convertToEntityAttribute(Integer dbAttrTypeId)
    {
        return AttrType.withId(dbAttrTypeId);
    }
}
