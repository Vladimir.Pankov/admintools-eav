package com.homemade.admintools.entities.eav.objects;

import java.math.BigInteger;

import com.google.common.base.Preconditions;

import com.homemade.admintools.entities.eav.objects.impl.RawObjectImpl;

public class RawObjectBuilder
{
    public RawObjectBuilder name(String name)
    {
        this.name = name;
        return this;
    }
    
    public RawObjectBuilder description(String description)
    {
        this.description = description;
        return this;
    }
    
    public RawObjectBuilder objectTypeId(BigInteger objectTypeId)
    {
        this.objectTypeId = objectTypeId;
        return this;
    }
    
    public RawObjectBuilder parentId(BigInteger parentId)
    {
        this.parentId = parentId;
        return this;
    }
    
    public RawObject build()
    {
        validateState();
        
        RawObject rawObject = new RawObjectImpl(objectTypeId);        
        rawObject.setName(name);
        if (parentId != null)
        {
            rawObject.setParentId(parentId);
        }
        if (description != null)
        {
            rawObject.setDescription(description);
        }        
        return rawObject;
    }
    
    private void validateState()
    {
        Preconditions.checkNotNull(objectTypeId, "Object Type ID cannot be null");
        Preconditions.checkNotNull(name, "Name cannot be null");
    }
    
    private String name;
    private String description;
    private BigInteger objectTypeId;
    private BigInteger parentId;
}
