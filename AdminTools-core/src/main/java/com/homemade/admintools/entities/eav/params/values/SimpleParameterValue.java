package com.homemade.admintools.entities.eav.params.values;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects.ToStringHelper;

import com.homemade.admintools.entities.eav.params.keys.ParameterKey;

import lombok.Getter;
import lombok.Setter;

import com.homemade.admintools.entities.eav.metadata.ListValue;
import com.homemade.admintools.entities.eav.metadata.impl.ListValueImpl;

@Entity
@Table(name = "PARAMS")
public class SimpleParameterValue extends BasicParameterValue implements Serializable
{      
    public SimpleParameterValue() { }
    
    public SimpleParameterValue(BigInteger attrId, BigInteger objectId)
    {        
        setKey(new ParameterKey(attrId, objectId));
    }
    
    @Override
    public String getStringValue()
    {
        return value;
    }
    
    @Override
    public void setStringValue(String value)
    {
        this.value = value;
    }

    @Override
    public BigInteger getReference()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setReference(BigInteger value)
    {
        throw new UnsupportedOperationException();
    }
    
    @Override
    protected void fillToStringHelper(ToStringHelper toStringHelper)
    {
        toStringHelper
            .add("value", getStringValue())
            .add("dateValue", getDateValue())
            .add("listValue", getListValue());
    }
    
    @Column(name = "VALUE")
    private String value;
    
    @Column(name = "DATE_VALUE")
    @Getter
    @Setter
    private Date dateValue;

    @ManyToOne(targetEntity = ListValueImpl.class)
    @JoinColumn(name = "LIST_VALUE_ID")
    @Getter
    @Setter
    private ListValue listValue;
    
    private static final long serialVersionUID = -4576408169313660710L;
}
