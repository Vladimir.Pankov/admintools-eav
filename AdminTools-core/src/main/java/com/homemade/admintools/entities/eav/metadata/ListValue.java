package com.homemade.admintools.entities.eav.metadata;

import com.homemade.admintools.entities.eav.BaseEntity;

public interface ListValue extends BaseEntity
{
    String getAdditional();
    
    void setAdditional(String additional);
    
    AttrTypeDef getAttrTypeDef();
    
    void setAttrTypeDef(AttrTypeDef attrTypeDef);
}
