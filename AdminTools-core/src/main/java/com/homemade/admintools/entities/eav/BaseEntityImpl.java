package com.homemade.admintools.entities.eav;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.GenericGenerator;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;
import com.google.common.base.Objects;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public abstract class BaseEntityImpl implements BaseEntity, Serializable
{          
    @Override
    public int hashCode()
    {
        return Objects.hashCode(getId());
    }
    
    @Override
    public boolean equals(Object obj)
    {        
        if (this == obj)
        {
            return true; 
        }
        if (obj == null)
        {
            return false;
        }
        if (!(obj instanceof BaseEntity))
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        BaseEntity other = (BaseEntity)obj;
        return Objects.equal(getId(), other.getId());
    }
    
    @Override
    public String toString()
    {
        ToStringHelper toStringHelper = MoreObjects.toStringHelper(this);
        fillToStringHelper(toStringHelper);
        return toStringHelper.toString();
    }
    
    protected void fillToStringHelper(ToStringHelper toStringHelper)
    {
        toStringHelper
            .add("id", getId())
            .add("name", getName());
    }
    
    @Id
    @GenericGenerator
    (
        name = "globalIdGenerator", 
        strategy = "com.homemade.admintools.entities.eav.utils.idgenerators.GlobalIdGenerator"
    )
    @GeneratedValue(generator = "globalIdGenerator")
    @Getter
    private BigInteger id;
    
    @Column(name = "NAME")
    @Getter
    @Setter
    private String name;
   
    private static final long serialVersionUID = 8845794534227469197L;
}
