package com.homemade.admintools.entities.eav.params.values;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.common.base.MoreObjects.ToStringHelper;

import com.homemade.admintools.entities.eav.params.keys.ParameterKey;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.homemade.admintools.entities.eav.metadata.ListValue;

@Entity
@Table(name = "OBJ_REFERENCES")
@NoArgsConstructor
public class ReferenceParameterValue extends BasicParameterValue implements Serializable
{     
    public ReferenceParameterValue(BigInteger attrId, BigInteger objectId)
    {
        setKey(new ParameterKey(attrId, objectId));
    }
    
    @Override
    public String getStringValue()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public Date getDateValue()
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListValue getListValue()
    {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public void setStringValue(String value)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setDateValue(Date value)
    {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setListValue(ListValue value)
    {
        throw new UnsupportedOperationException();
    } 
    
    @Override
    protected void fillToStringHelper(ToStringHelper toStringHelper)
    {
        toStringHelper.add("reference", getReference());
    }
    
    @Column(name = "REFERENCE")
    @Getter
    @Setter
    private BigInteger reference;
    
    private static final long serialVersionUID = 4284943552734634958L;
}
