package com.homemade.admintools.entities.eav.objects;

import java.math.BigInteger;
import java.util.List;

import com.homemade.admintools.entities.eav.params.ModifiableParameterValue;

public interface RawObject extends SimpleObject
{
    RawObject getParent();
    
    BigInteger getParentId();
    
    void setParentId(BigInteger parentId);
    
    List<ModifiableParameterValue> getParamValues(BigInteger attrId);

    List<? extends ModifiableParameterValue> getSimpleParamValues();  
    
    List<? extends ModifiableParameterValue> getRefParamValues();
}
