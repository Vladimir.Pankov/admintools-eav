package com.homemade.admintools.entities.eav.metadata.impl.keys;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
public class AttrToObjectTypeBindingKey implements Serializable
{      
    @Column(name = "ATTR_ID")
    private BigInteger attrId;
    
    @Column(name = "OBJECT_TYPE_ID")
    private BigInteger objectTypeId;
    
    private static final long serialVersionUID = 6965021772540664305L;
}
