package com.homemade.admintools.entities.eav.metadata;

public enum BindType
{
    BOUND(0),
    UNBOUND(1);
    
    public int getId()
    {
        return id;
    }
    
    public static BindType withId(int id)
    {
        for (BindType bindType : BindType.values())
        {
            if (bindType.getId() == id)
            {
                return bindType;
            }
        }
        return null;
    }
    
    BindType(int id)
    {
        this.id = id;
    }
    
    private int id;
}
