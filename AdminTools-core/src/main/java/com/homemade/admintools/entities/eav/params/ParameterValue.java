package com.homemade.admintools.entities.eav.params;

import java.math.BigInteger;
import java.util.Date;

import com.homemade.admintools.entities.eav.metadata.ListValue;

public interface ParameterValue
{
    BigInteger getAttrId();
    
    BigInteger getObjectId();
    
    String getStringValue();
    
    Date getDateValue();
   
    ListValue getListValue();
    
    BigInteger getReference(); 
    
    <T> T getValue(Class<T> valueType);
}
