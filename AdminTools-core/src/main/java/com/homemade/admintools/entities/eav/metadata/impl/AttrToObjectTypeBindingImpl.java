package com.homemade.admintools.entities.eav.metadata.impl;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.Objects;
import com.homemade.admintools.entities.eav.metadata.AttrToObjectTypeBinding;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.metadata.BindType;
import com.homemade.admintools.entities.eav.metadata.impl.keys.AttrToObjectTypeBindingKey;
import com.homemade.admintools.entities.eav.utils.converters.IntToBindTypeConverter;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ATTR_OBJECT_TYPES")
public class AttrToObjectTypeBindingImpl implements AttrToObjectTypeBinding
{
    @Override
    public BigInteger getAttrId()
    {
        return key.getAttrId();
    }

    @Override
    public BigInteger getObjectTypeId()
    {
        return key.getObjectTypeId();
    }

    @Override
    public int hashCode()
    {
        return Objects.hashCode(key);
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
        {
            return true;
        }
        if (obj == null)
        {
            return false;
        }
        if (getClass() != obj.getClass())
        {
            return false;
        }
        AttrToObjectTypeBindingImpl other = (AttrToObjectTypeBindingImpl)obj;
        return Objects.equal(key, other.key);
    }
    
    @Id
    private AttrToObjectTypeBindingKey key;
    
    @Column(name = "BIND_TYPE")
    @Convert(converter = IntToBindTypeConverter.class)
    @Getter
    @Setter
    private BindType bindType;
    
    @ManyToOne(targetEntity = AttributeImpl.class)
    @JoinColumn(name = "ATTR_ID", insertable = false, updatable = false)
    @Getter
    private Attribute attribute;
}
