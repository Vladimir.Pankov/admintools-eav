package com.homemade.admintools.entities.eav.metadata;

import java.util.Set;

import com.homemade.admintools.entities.eav.BaseEntity;

public interface AttrTypeDef extends BaseEntity
{
    String getDescription();
    
    void setDescription(String description);
    
    Set<ListValue> getListValues();
    
    void setListValues(Set<ListValue> listValues);
}
