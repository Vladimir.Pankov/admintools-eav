package com.homemade.admintools.entities.eav.metadata.impl;

import java.util.Set;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.homemade.admintools.entities.eav.BaseEntityImpl;
import com.homemade.admintools.entities.eav.metadata.AttrTypeDef;
import com.homemade.admintools.entities.eav.metadata.ListValue;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ATTR_TYPE_DEFS")
@AttributeOverride(name = "id", column = @Column(name = "ATTR_TYPE_DEF_ID"))
@Getter
@Setter
public class AttrTypeDefImpl extends BaseEntityImpl implements AttrTypeDef
{    
    @Column(name = "DESCRIPTION")
    private String description;
    
    @OneToMany(targetEntity = ListValueImpl.class, mappedBy = "attrTypeDef")
    private Set<ListValue> listValues;
    
    private static final long serialVersionUID = 4919487544414917262L;
}
