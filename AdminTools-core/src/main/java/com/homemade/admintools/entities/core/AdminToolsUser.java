package com.homemade.admintools.entities.core;

import java.math.BigInteger;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.base.MoreObjects;
import com.google.common.collect.Sets;

import com.homemade.admintools.entities.eav.objects.EnrichedObject;

public class AdminToolsUser
{
    public static final BigInteger OBJECT_TYPE_ID = BigInteger.valueOf(9000149295739835221L);
    public static final BigInteger PASSWORD_ATTR_ID = BigInteger.valueOf(9000149295767978668L);
    
    public AdminToolsUser(EnrichedObject enrichedObject)
    {
        this.enrichedObject = enrichedObject;
    }
    
    public BigInteger getId()
    {
        return enrichedObject.getId();
    }
    
    public String getUserName()
    {
        return enrichedObject.getName();
    }
    
    public void setUserName(String userName)
    {
        enrichedObject.setName(userName);
    }
    
    public String getPassword()
    {
        return enrichedObject.getParameter(PASSWORD_ATTR_ID).getStringValue();
    }
    
    public void setPassword(String password)
    {
        enrichedObject.getParameter(PASSWORD_ATTR_ID).setValue(password);
    }
    
    public Set<? extends GrantedAuthority> getAuthorities()
    {
        return DEFAULT_AUTHORITIES;
    }
    
    @Override
    public String toString()
    {
        return MoreObjects.toStringHelper(this)
                .add("userName", getUserName())
                .add("password", getPassword())
                .toString();
    }

    private final EnrichedObject enrichedObject;
    
    private static final Set<? extends GrantedAuthority> DEFAULT_AUTHORITIES = 
        Sets.newHashSet(new SimpleGrantedAuthority("ROLE_USER"));
}
