package com.homemade.admintools.entities.eav.metadata.impl;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.homemade.admintools.entities.eav.BaseEntityImpl;
import com.homemade.admintools.entities.eav.metadata.AttrType;
import com.homemade.admintools.entities.eav.metadata.AttrTypeDef;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.utils.converters.IntToAttrTypeConverter;
import com.homemade.admintools.entities.eav.utils.converters.IntToBooleanConverter;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "ATTRIBUTES")
@AttributeOverride(name = "id", column = @Column(name = "ATTR_ID"))
@Getter
@Setter
public class AttributeImpl extends BaseEntityImpl implements Attribute
{   
    @Column(name = "IS_MULTIPLE")
    @Convert(converter = IntToBooleanConverter.class)
    private boolean multiple;
    
    @Column(name = "IS_CALCULABLE")
    @Convert(converter = IntToBooleanConverter.class)
    private boolean calculable;
    
    @Column(name = "PROPERTIES")
    private String properties;
    
    @Column(name = "ATTR_TYPE_ID")
    @Convert(converter = IntToAttrTypeConverter.class)
    private AttrType type;
    
    @ManyToOne(targetEntity = AttrTypeDefImpl.class)
    @JoinColumn(name = "ATTR_TYPE_DEF_ID")
    private AttrTypeDef attrTypeDef;
    
    private static final long serialVersionUID = 3082764965706002651L;
}