package com.homemade.admintools.entities.eav.metadata;

import com.homemade.admintools.entities.eav.BaseEntity;

public interface Attribute extends BaseEntity
{
    public boolean isMultiple();

    public void setMultiple(boolean multipleFlag);

    public boolean isCalculable();

    public void setCalculable(boolean calculableFlag);

    public String getProperties();

    public void setProperties(String properties);

    public AttrType getType();

    public void setType(AttrType attrType);

    public AttrTypeDef getAttrTypeDef();

    public void setAttrTypeDef(AttrTypeDef attrTypeDef);
}
