package com.homemade.admintools.entities.eav.params;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import com.homemade.admintools.dao.eav.RawObjectParameterValueDAO;
import com.homemade.admintools.entities.eav.objects.RawObject;
import com.homemade.admintools.entities.eav.metadata.ListValue;
import com.homemade.admintools.spring.BeansHolder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ParameterImpl implements Parameter
{
    public ParameterImpl(BigInteger attrId, RawObject objectEntity)
    {
        this.attrId = attrId;
        this.objectEntity = objectEntity;
        this.paramValues.addAll(objectEntity.getParamValues(attrId));
        this.objectParamDAO = BeansHolder.getInstance().getRawObjectParameterValueDAO(); 
    }
    
    @Override
    public BigInteger getAttrId()
    {
        return attrId;
    }

    @Override
    public BigInteger getObjectId()
    {
        return objectEntity.getId();
    }

    @Override
    public String getStringValue()
    {
        return getSingleValue(String.class);
    }

    @Override
    public Date getDateValue()
    {
        return getSingleValue(Date.class);
    }

    @Override
    public ListValue getListValue()
    {
        return getSingleValue(ListValue.class);
    }

    @Override
    public BigInteger getReference()
    {
        return getSingleValue(BigInteger.class);
    }

    @Override
    public <T> T getValue(Class<T> valueType)
    {
        return getSingleValue(valueType);
    }

    @Override
    public Collection<String> getStringValues()
    {
        return getMultipleValues(String.class);
    }

    @Override
    public Collection<Date> getDateValues()
    {
        return getMultipleValues(Date.class);
    }

    @Override
    public Collection<ListValue> getListValues()
    {
        return getMultipleValues(ListValue.class);
    }

    @Override
    public Collection<BigInteger> getReferences()
    {
        return getMultipleValues(BigInteger.class);
    }

    @Override
    public void setValue(Object value)
    {        
        ModifiableParameterValue paramValue = Iterables.getFirst(paramValues, null);
        if (paramValue == null)
        {
            paramValue = objectParamDAO.create(getAttrId(), getObjectId());
            
            log.debug("paramValue: {}", paramValue);
            
            paramValues.add(paramValue);
        }
        paramValue.setValue(value);
    }

    @Override
    public void setValues(Collection<?> values)
    {
        
    }
    
    @Override
    public void setValues(Object... values)
    {
        setValues(values == null ? Collections.emptyList() : Arrays.asList(values));
    }
    
    private <T> T getSingleValue(Class<T> valueType)
    {
        ModifiableParameterValue paramValue = Iterables.getFirst(paramValues, null);
        if (paramValue == null)
        {
            return null;
        }
        return paramValue.getValue(valueType);
    }
    
    private <T> Collection<T> getMultipleValues(Class<T> valueType)
    {
        Collection<T> values = Lists.newArrayList();
        for (ModifiableParameterValue paramValue : paramValues)
        {
            values.add(paramValue.getValue(valueType));
        }
        return values;
    }
    
    private final BigInteger attrId;
    private final RawObject objectEntity;
    private final RawObjectParameterValueDAO objectParamDAO;
    private final List<ModifiableParameterValue> paramValues = Lists.newArrayList();
}
