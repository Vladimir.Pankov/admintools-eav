package com.homemade.admintools.entities.eav;

import java.math.BigInteger;

public interface BaseEntity
{
    BigInteger getId();
    
    String getName();
    
    void setName(String name);
}