package com.homemade.admintools.entities.eav.objects;

import java.math.BigInteger;

import com.homemade.admintools.entities.eav.params.Parameter;

public interface EnrichedObject extends SimpleObject
{
    EnrichedObject getParent();
    
    Parameter getParameter(BigInteger attrId);
    
    RawObject getRawObject();
}
