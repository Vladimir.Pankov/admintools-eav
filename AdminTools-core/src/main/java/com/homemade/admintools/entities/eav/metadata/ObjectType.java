package com.homemade.admintools.entities.eav.metadata;

import java.util.Set;

import com.homemade.admintools.entities.eav.BaseEntity;

public interface ObjectType extends BaseEntity
{
    ObjectType getParentObjectType();
    
    void setParentObjectType(ObjectType parentObjectType);
    
    Set<ObjectType> getDescendants();
    
    Set<Attribute> getAttributes();
    
    Set<Attribute> getAttributesWithInherited();
}
