package com.homemade.admintools.entities.eav.params;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Date;

import com.homemade.admintools.entities.eav.metadata.ListValue;

public interface Parameter extends ParameterValue
{    
    Collection<String> getStringValues();
    
    Collection<Date> getDateValues();
    
    Collection<ListValue> getListValues();
    
    Collection<BigInteger> getReferences();
    
    void setValue(Object value);
    
    void setValues(Collection<?> values);
    
    void setValues(Object... values);
}
