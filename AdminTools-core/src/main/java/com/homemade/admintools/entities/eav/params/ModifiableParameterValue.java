package com.homemade.admintools.entities.eav.params;

import java.math.BigInteger;
import java.util.Date;

import com.homemade.admintools.entities.eav.metadata.ListValue;

public interface ModifiableParameterValue extends ParameterValue
{
    void setStringValue(String value);
    
    void setDateValue(Date value);
    
    void setListValue(ListValue value);
    
    void setReference(BigInteger value);
    
    void setValue(Object value);
}
