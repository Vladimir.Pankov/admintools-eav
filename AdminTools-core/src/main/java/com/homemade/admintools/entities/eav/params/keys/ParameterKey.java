package com.homemade.admintools.entities.eav.params.keys;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Embeddable
@Getter @Setter @EqualsAndHashCode @ToString @NoArgsConstructor 
@AllArgsConstructor
public class ParameterKey implements Serializable
{
    @Column(name = "ATTR_ID")
    private BigInteger attrId;
    
    @Column(name = "OBJECT_ID")
    private BigInteger objectId;
    
    private static final long serialVersionUID = -3118780465450996235L;
}
