package com.homemade.admintools.common;

import java.math.BigInteger;

public final class EAVConstants
{
    public static final BigInteger ROOT_OT_ID = BigInteger.ONE;
    
    private EAVConstants() { }
}
