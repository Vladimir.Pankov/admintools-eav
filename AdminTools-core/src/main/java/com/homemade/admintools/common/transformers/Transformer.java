package com.homemade.admintools.common.transformers;

public interface Transformer<S, T>
{
    T transform(S source);
}
