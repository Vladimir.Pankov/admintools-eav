package com.homemade.admintools.dao.auth;

import javax.inject.Inject;
import javax.inject.Named;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.homemade.admintools.dao.eav.EnrichedObjectDAO;
import com.homemade.admintools.entities.core.AdminToolsUser;
import com.homemade.admintools.entities.eav.objects.RawObject;
import com.homemade.admintools.entities.eav.objects.impl.RawObjectImpl;

import lombok.extern.slf4j.Slf4j;

@Named("userDAO")
@Slf4j
public class UserDAO
{
    @Inject
    public UserDAO(EnrichedObjectDAO enrichedObjectDAO,
                   PasswordEncoder passwordEncoder)
    {
        this.enrichedObjectDAO = enrichedObjectDAO;
        this.passwordEncoder = passwordEncoder;
    }
    
    @Transactional(propagation = Propagation.MANDATORY)
    public AdminToolsUser findByUserName(String userName)
    {        
        TypedQuery<? extends RawObject> query = entityManager.createNamedQuery(
                "AdminToolsUser.findByUserName", RawObjectImpl.class);
        query.setMaxResults(1);
        query.setParameter("name", userName);
        
        AdminToolsUser user = null;
        try
        {
            RawObject userObject = query.getSingleResult();
            
            log.debug("User Object found: {}", userObject);
            
            user = new AdminToolsUser(enrichedObjectDAO.enrich(userObject));
        }
        catch (NoResultException e) 
        {
        	log.info("User with name = '{}' not found", userName);
        }
        return user;
    }
    
    @Transactional(propagation = Propagation.MANDATORY)
    public AdminToolsUser create(String userName, String password)
    {
        log.debug("Creating user with name = {}", userName);
        
        AdminToolsUser user = new AdminToolsUser(
            enrichedObjectDAO.create(AdminToolsUser.OBJECT_TYPE_ID, null, userName));        
        user.setPassword(passwordEncoder.encode(password));
        return user;
    }
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private final EnrichedObjectDAO enrichedObjectDAO;
    private final PasswordEncoder passwordEncoder;
}
