package com.homemade.admintools.dao.eav;

import java.math.BigInteger;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import com.homemade.admintools.entities.eav.objects.EnrichedObject;
import com.homemade.admintools.entities.eav.objects.RawObject;
import com.homemade.admintools.entities.eav.objects.RawObjectBuilder;
import com.homemade.admintools.entities.eav.objects.impl.EnrichedObjectImpl;
import com.homemade.admintools.entities.eav.objects.impl.RawObjectImpl;

@Named("enrichedObjectDAO")
public class EnrichedObjectDAO
{
    @Transactional(propagation = Propagation.MANDATORY)
    public EnrichedObject findById(BigInteger objectId)
    {
        Preconditions.checkNotNull(objectId, "Cannot find Object by ID = null");
        return enrich(entityManager.find(RawObjectImpl.class, objectId));
    }
    
    @Transactional(propagation = Propagation.MANDATORY)
    public EnrichedObject create(BigInteger objectTypeId, BigInteger parentId, String name)
    {
        RawObject rawObject = new RawObjectBuilder()
                .objectTypeId(objectTypeId)
                .parentId(parentId)
                .name(name)
                .build();
        entityManager.persist(rawObject);
        return enrich(rawObject);
    }
    
    public EnrichedObject enrich(RawObject rawObject)
    {        
        return new EnrichedObjectImpl(rawObject);
    }
    
    @PersistenceContext
    private EntityManager entityManager;
}
