package com.homemade.admintools.dao.eav;

import java.math.BigInteger;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.metadata.impl.AttributeImpl;

@Named("attributeDAO")
public class AttributeDAO
{
    @Transactional(propagation = Propagation.MANDATORY)
    public Attribute findById(BigInteger attrId)
    {
        Preconditions.checkNotNull(attrId, "Cannot find Attribute by ID = null");
        return entityManager.find(AttributeImpl.class, attrId);
    }
    
    @PersistenceContext
    private EntityManager entityManager;
}
