package com.homemade.admintools.dao.eav;

import java.math.BigInteger;

import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import com.homemade.admintools.entities.eav.metadata.AttrType;
import com.homemade.admintools.entities.eav.metadata.Attribute;
import com.homemade.admintools.entities.eav.params.ModifiableParameterValue;
import com.homemade.admintools.entities.eav.params.values.ReferenceParameterValue;
import com.homemade.admintools.entities.eav.params.values.SimpleParameterValue;

@Named("rawObjectParameterValueDAO")
public class RawObjectParameterValueDAO
{
    @Inject
    public RawObjectParameterValueDAO(AttributeDAO attributeDAO)
    {
        this.attributeDAO = attributeDAO;
    }
    
    @Transactional(propagation = Propagation.MANDATORY)
    public ModifiableParameterValue create(BigInteger attrId, BigInteger objectId)
    {
        Preconditions.checkNotNull(attrId, "Cannot create parameter value of Attribute with ID = null");
        Preconditions.checkNotNull(objectId, "Cannot create parameter value of Object with ID = null");
        
        Attribute attr = attributeDAO.findById(attrId);
        ModifiableParameterValue paramValue = attr.getType() == AttrType.REFERENCE ?
            new ReferenceParameterValue(attrId, objectId) : 
            new SimpleParameterValue(attrId, objectId);
        entityManager.persist(paramValue);
        return paramValue;
    }
    
    private final AttributeDAO attributeDAO;
    
    @PersistenceContext
    private EntityManager entityManager;
}
