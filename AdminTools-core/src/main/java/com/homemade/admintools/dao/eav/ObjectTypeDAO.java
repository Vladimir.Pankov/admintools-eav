package com.homemade.admintools.dao.eav;

import java.math.BigInteger;

import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.Preconditions;

import com.homemade.admintools.entities.eav.metadata.ObjectType;
import com.homemade.admintools.entities.eav.metadata.impl.ObjectTypeImpl;

@Named("objectTypeDAO")
public class ObjectTypeDAO
{
    @Transactional(propagation = Propagation.MANDATORY)
    public ObjectType findById(BigInteger objectTypeId)
    {
        Preconditions.checkNotNull(objectTypeId, "Cannot find Object Type by ID = null");
        return entityManager.find(ObjectTypeImpl.class, objectTypeId);
    }    
    
    @Transactional(propagation = Propagation.MANDATORY)
    public ObjectType create(String name, BigInteger parentId)
    {
        Preconditions.checkNotNull(name, "Cannot create an Object Type with empty name");
        Preconditions.checkNotNull(parentId, "Cannot create an Object Type without parent");
        
        ObjectType objectType = new ObjectTypeImpl();
        objectType.setName(name);
        objectType.setParentObjectType(findById(parentId));
        entityManager.persist(objectType);
        return objectType;
    }
    
    @Transactional(propagation = Propagation.MANDATORY)
    public void deleteById(BigInteger objectTypeId)
    {
        Preconditions.checkNotNull(objectTypeId, "Object Type ID = null is not allowed");
        
        delete(findById(objectTypeId));
    }
    
    @Transactional(propagation = Propagation.MANDATORY)
    public void delete(ObjectType objectType)
    {
        Preconditions.checkNotNull(objectType, "Object Type = null is not allowed");
        
        entityManager.remove(objectType);
    }
    
    @PersistenceContext
    private EntityManager entityManager;
}
