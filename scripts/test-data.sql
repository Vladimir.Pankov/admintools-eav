insert into object_types(object_type_id, name, parent_id)
values
(
  2, 'Test OT', 1
);

insert into attributes(attr_id, name, attr_type_id, is_multiple, is_calculable)
values
(
  101, 'Test Attr 1', 1, 0, 0
);

insert into attributes(attr_id, name, attr_type_id, is_multiple, is_calculable)
values
(
  102, 'Test Attr 2', 1, 0, 0
);

insert into attr_groups(attr_group_id, name)
values
(
  300, 'Test Attr Group'
);

insert into attr_object_types(attr_id, object_type_id, bind_type, attr_group_id)
values
(
  101, 2, 0, 300
);

insert into attr_object_types(attr_id, object_type_id, bind_type, attr_group_id)
values
(
  102, 2, 0, 300
);

insert into objects(object_id, name, object_type_id, parent_id, description)
values
(
  10, 'Test Object', 2, null, 'test object description'
);

insert into params(attr_id, object_id, value)
values
(
  101, 10, 'abc'
);

insert into params(attr_id, object_id, value)
values
(
  102, 10, 'abc567'
);