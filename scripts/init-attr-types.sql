insert into attr_types(attr_type_id, attr_type_name)
values
(
  1, 'Text'
);

insert into attr_types(attr_type_id, attr_type_name)
values
(
  2, 'Number'
);

insert into attr_types(attr_type_id, attr_type_name)
values
(
  3, 'Date'
);

insert into attr_types(attr_type_id, attr_type_name)
values
(
  4, 'List'
);

insert into attr_types(attr_type_id, attr_type_name)
values
(
  5, 'Reference'
);