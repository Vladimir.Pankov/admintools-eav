CREATE OR REPLACE FUNCTION get_id()
  RETURNS bigint AS
$BODY$
    BEGIN
        return 9000000000000000000 + cast(extract(epoch from now()) * 100000 as bigint);
    END
    $BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
  
ALTER FUNCTION get_id()
  OWNER TO postgres;