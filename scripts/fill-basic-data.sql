alter table object_types 
alter parent_id 
  drop not null;

insert into object_types(object_type_id, name)
values
(
  1, 
  'Root'
);

alter table object_types 
  add constraint non_null_parent_ot 
    check (parent_id is not null) 
    not valid;
    
insert into object_types(object_type_id, name, parent_id)
values
(
  9000149295739835221,
  'User',
  1
);

insert into attributes(attr_id, name, attr_type_id, is_multiple, is_calculable)
values
(
  9000149295767978668,
  'Password',
  1,
  0,
  0
);

insert into attr_object_types(attr_id, object_type_id, bind_type)
values
(
  9000149295767978668,
  9000149295739835221,
  0
);

