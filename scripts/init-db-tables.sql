SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

CREATE TABLE attr_groups (
    attr_group_id bigint NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE attr_groups OWNER TO postgres;

CREATE TABLE attr_object_types (
    object_type_id bigint NOT NULL,
    attr_id bigint NOT NULL,
    bind_type integer NOT NULL,
    schema_id bigint,
    default_value character varying,
    attr_group_id bigint
);


ALTER TABLE attr_object_types OWNER TO postgres;

CREATE TABLE attr_type_defs (
    attr_type_def_id bigint NOT NULL,
    name character varying NOT NULL,
    description character varying
);


ALTER TABLE attr_type_defs OWNER TO postgres;

CREATE TABLE attr_types (
    attr_type_id bigint NOT NULL,
    attr_type_name character varying NOT NULL
);


ALTER TABLE attr_types OWNER TO postgres;

CREATE TABLE attributes (
    attr_id bigint NOT NULL,
    name character varying NOT NULL,
    attr_type_id bigint NOT NULL,
    is_multiple integer NOT NULL,
    is_calculable integer NOT NULL,
    properties character varying,
    attr_type_def_id bigint
);


ALTER TABLE attributes OWNER TO postgres;

CREATE TABLE list_values (
    list_value_id bigint NOT NULL,
    name character varying NOT NULL,
    additional character varying,
    attr_type_def_id bigint NOT NULL
);


ALTER TABLE list_values OWNER TO postgres;

CREATE TABLE meta_references (
    attr_id bigint NOT NULL,
    object_id bigint NOT NULL,
    ref_attr_id bigint,
    ref_object_type_id bigint
);


ALTER TABLE meta_references OWNER TO postgres;

CREATE TABLE objects (
    object_id bigint NOT NULL,
    object_type_id bigint NOT NULL,
    name character varying NOT NULL,
    parent_id bigint,
    description character varying
);


ALTER TABLE objects OWNER TO postgres;

CREATE TABLE object_types (
    object_type_id bigint NOT NULL,
    name character varying NOT NULL,
    parent_id bigint NOT NULL
);


ALTER TABLE object_types OWNER TO postgres;

CREATE TABLE params (
    attr_id bigint NOT NULL,
    object_id bigint NOT NULL,
    value character varying,
    date_value timestamp without time zone,
    list_value_id bigint
);


ALTER TABLE params OWNER TO postgres;

CREATE TABLE obj_references (
    attr_id bigint NOT NULL,
    object_id bigint NOT NULL,
    reference bigint NOT NULL
);

-- Primary keys

ALTER TABLE obj_references OWNER TO postgres;

ALTER TABLE ONLY attr_groups
    ADD CONSTRAINT attr_groups_pk PRIMARY KEY (attr_group_id);

ALTER TABLE ONLY attr_object_types
    ADD CONSTRAINT attr_object_types_pk PRIMARY KEY (object_type_id, attr_id);

ALTER TABLE ONLY attr_type_defs
    ADD CONSTRAINT attr_type_defs_pk PRIMARY KEY (attr_type_def_id);

ALTER TABLE ONLY attr_types
    ADD CONSTRAINT attr_types_pk PRIMARY KEY (attr_type_id);

ALTER TABLE ONLY attributes
    ADD CONSTRAINT attributes_pk PRIMARY KEY (attr_id);

ALTER TABLE ONLY list_values
    ADD CONSTRAINT list_values_pk PRIMARY KEY (list_value_id);

ALTER TABLE ONLY meta_references
    ADD CONSTRAINT meta_references_pk PRIMARY KEY (attr_id, object_id);

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects_pk PRIMARY KEY (object_id);

ALTER TABLE ONLY object_types
    ADD CONSTRAINT object_types_pk PRIMARY KEY (object_type_id);

ALTER TABLE ONLY obj_references
    ADD CONSTRAINT obj_references_pk PRIMARY KEY (attr_id, object_id, reference);

CREATE INDEX ref_values_index ON obj_references USING btree (attr_id, object_id);
CREATE INDEX back_ref_values_index ON obj_references USING btree (attr_id, reference);
CREATE INDEX param_values_index ON params USING btree (attr_id, object_id);

-- Foreign keys

ALTER TABLE ONLY attr_object_types
    ADD CONSTRAINT attr_obj_types2obj_type_fk FOREIGN KEY (object_type_id) REFERENCES object_types(object_type_id) on delete cascade;

ALTER TABLE ONLY attr_object_types
    ADD CONSTRAINT attr_obj_types2attr_fk FOREIGN KEY (attr_id) REFERENCES attributes(attr_id) on delete cascade;

ALTER TABLE ONLY attr_object_types
    ADD CONSTRAINT attr_obj_types2attr_group_fk FOREIGN KEY (attr_group_id) REFERENCES attr_groups(attr_group_id) on delete cascade;

ALTER TABLE ONLY list_values
    ADD CONSTRAINT list_value2attr_type_def_fk FOREIGN KEY (attr_type_def_id) REFERENCES attr_type_defs(attr_type_def_id) on delete cascade;


ALTER TABLE ONLY meta_references
    ADD CONSTRAINT meta_ref2attr_fk FOREIGN KEY (attr_id) REFERENCES attributes(attr_id) on delete cascade;

ALTER TABLE ONLY meta_references
    ADD CONSTRAINT meta_ref2obj_fk FOREIGN KEY (object_id) REFERENCES objects(object_id) on delete cascade;

ALTER TABLE ONLY meta_references
    ADD CONSTRAINT meta_ref2ref_attr_fk FOREIGN KEY (ref_attr_id) REFERENCES attributes(attr_id) on delete cascade;

ALTER TABLE ONLY meta_references
    ADD CONSTRAINT meta_ref2obj_type_fk FOREIGN KEY (ref_object_type_id) REFERENCES object_types(object_type_id) on delete cascade;

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects2obj_type_fk FOREIGN KEY (object_type_id) REFERENCES object_types(object_type_id) on delete cascade;

ALTER TABLE ONLY objects
    ADD CONSTRAINT objects2parent_obj_fk FOREIGN KEY (parent_id) REFERENCES objects(object_id) on delete cascade;

ALTER TABLE ONLY object_types
    ADD CONSTRAINT obj_types2parent_ot_fk FOREIGN KEY (parent_id) REFERENCES object_types(object_type_id) on delete cascade;

ALTER TABLE ONLY params
    ADD CONSTRAINT params2attr_fk FOREIGN KEY (attr_id) REFERENCES attributes(attr_id) on delete cascade;

ALTER TABLE ONLY params
    ADD CONSTRAINT params2obj_fk FOREIGN KEY (object_id) REFERENCES objects(object_id) on delete cascade;

ALTER TABLE ONLY params
    ADD CONSTRAINT params_list_value_id_fkey FOREIGN KEY (list_value_id) REFERENCES list_values(list_value_id);

ALTER TABLE ONLY obj_references
    ADD CONSTRAINT references2attr_fk FOREIGN KEY (attr_id) REFERENCES attributes(attr_id) ON DELETE CASCADE;

ALTER TABLE ONLY obj_references
    ADD CONSTRAINT references2source_obj_fk FOREIGN KEY (object_id) REFERENCES objects(object_id) ON DELETE CASCADE;

ALTER TABLE ONLY obj_references
    ADD CONSTRAINT references2ref_obj_fk FOREIGN KEY (reference) REFERENCES objects(object_id) ON DELETE CASCADE;

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;