#!/usr/bin/env bash

echo "---------------------------------------------"
echo "Setting up Java EE Development Environment..."
echo "---------------------------------------------"

VAGRANT_DIR=/vagrant
DIST_DIR=${VAGRANT_DIR}/dist

function isPkgInstalled()
{
  pkgStatus=$(dpkg-query -W -f='${Status}\n' $1)
  [ "${pkgStatus}" == "install ok installed" ]
  return $?
}

function download()
{
  [ -f ${DIST_DIR}/$1 ] || wget -nv -O ${DIST_DIR}/"$@" ;
}

# Create folder for software distribution files
sudo mkdir -p ${DIST_DIR}

# Initial repositories update
sudo apt-get update

# Install system tools
sudo apt-get install -y python-software-properties

# Add external package repositories
sudo add-apt-repository ppa:webupd8team/java
sudo add-apt-repository ppa:openjdk-r/ppa

# Add PostgreSQL repository file
if [ ! -f /etc/apt/sources.list.d/pgdg.list ]
then
  echo "Adding PostgreSQL repository file..."
  cat > /etc/apt/sources.list.d/pgdg.list << EOF
deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main
EOF
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | \
    sudo apt-key add -
  sudo apt-get update
fi

# Install useful tools
sudo apt-get install -y mc

# Install JDK 8
jdkPackage="openjdk-8-jdk" # oracle-java8-installer|openjdk-8-jdk

# JAVA_HOME
javaHome=""
if [ "${jdkPackage}" == "oracle-java8-installer" ]
then
  javaHome="/usr/lib/jvm/java-8-oracle"
else
  javaHome="/usr/lib/jvm/java-8-openjdk-amd64"
fi

if ! isPkgInstalled ${jdkPackage}
then
  echo "-------------------"
  echo "Installing JDK 8..."
  echo "-------------------"
  if [ "${jdkPackage}" == "oracle-java8-installer" ]
  then
    echo ${jdkPackage} shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
    sudo apt-get install -y ${jdkPackage}
    sudo apt-get install -y oracle-java8-set-default
  else
    sudo apt-get install -y --force-yes ${jdkPackage}
    sudo update-alternatives --config java
    sudo update-alternatives --config javac
    sudo tee -a cat /etc/bash.bashrc > /dev/null << EOF
# Java environment variables
export JAVA_HOME=${javaHome}
EOF
  fi
  echo "-------------------------------------"
  echo "${jdkPackage} installed successfully!"
  echo "-------------------------------------"
else
  echo "---------------------------------------------------------"
  echo "${jdkPackage} is already installed. Skipping this step..."
  echo "---------------------------------------------------------"
fi

# Default credentials :)
ultimateUserName=admin
ultimateUserPassword=1

# Install PostgreSQL
postgresVersion="9.4"
if ! isPkgInstalled "postgresql-${postgresVersion}"
then
  echo "------------------------"
  echo "Installing PostgreSQL..."
  echo "------------------------"
  sudo apt-get install -y postgresql-${postgresVersion} postgresql-contrib-${postgresVersion} postgresql-client-${postgresVersion}

  # Configure PostgreSQL

  echo "-------------------------"
  echo "Configuring PostgreSQL..."
  echo "-------------------------"
  # Set PostgreSQL locale and character encoding
  sudo service postgresql stop
  sudo pg_dropcluster --stop ${postgresVersion} main
  sudo pg_createcluster --locale en_US.UTF-8 ${postgresVersion} main
  # Allow host OS connections
  sudo tee -a cat /etc/postgresql/*/main/postgresql.conf > /dev/null << EOF
# Custom configuration
listen_addresses = '*'
EOF
  sudo tee -a cat /etc/postgresql/*/main/pg_hba.conf > /dev/null << EOF
# Custom configuration
host    all             all               10.0.2.2/32             md5
EOF

  # Restart PostgreSQL service
  sudo service postgresql start

  # Create superuser 'admin' with password '1'
  sudo -u postgres psql -c "create role ${ultimateUserName} superuser createdb createrole login password '${ultimateUserPassword}'"

  echo "-----------------------------------------------------"
  echo "PostgreSQL ${postgresVersion} installed successfully!"
  echo "-----------------------------------------------------"
else
  echo "-------------------------------------------------------------------------"
  echo "PostgreSQL ${postgresVersion} is already installed. Skipping this step..."
  echo "-------------------------------------------------------------------------"
fi

# Install Wildfly 10.1.0-Final
jbossVersion="10.1.0.Final"
if ! service --status-all | grep 'wildfly'
then
  echo "---------------------"
  echo "Installing Wildfly..."
  echo "---------------------"
  jbossHome="/opt/wildfly"
  wildflyPkgName="wildfly-${jbossVersion}"
  wildflyZipPkgName="${wildflyPkgName}.zip"

  download ${wildflyZipPkgName} http://download.jboss.org/wildfly/${jbossVersion}/${wildflyZipPkgName}
  sudo unzip ${DIST_DIR}/${wildflyZipPkgName} -d /opt/
  sudo ln -s /opt/${wildflyPkgName} ${jbossHome}

  # Create directory for logs
  sudo mkdir -p /var/log/wildfly

  # Add access grants
  sudo chown -R vagrant:vagrant /opt/${wildflyPkgName}/
  sudo chown -R vagrant:vagrant ${jbossHome}
  sudo chown -R vagrant:vagrant /var/log/wildfly

  # Configure Wildfly server as a service
  sudo cp ${jbossHome}/docs/contrib/scripts/init.d/wildfly.conf /etc/default/wildfly
  sudo tee -a cat /etc/default/wildfly > /dev/null << EOF
JAVA_HOME=${javaHome}
JBOSS_HOME=${jbossHome}
JBOSS_USER="vagrant"
JBOSS_MODE="standalone"
JBOSS_CONFIG="standalone.xml"
JBOSS_CONSOLE_LOG="/var/log/wildfly/console.log"
STARTUP_WAIT=60
SHUTDOWN_WAIT=60
EOF
  debianInitScriptPath=${jbossHome}/docs/contrib/scripts/init.d/wildfly-init-debian.sh
  sudo sed -i -e 's,\[ $status_start -eq 3 \],\[ $status_start -eq 3 \] || \[ $status_start -eq 4 \],g' ${debianInitScriptPath}
  sudo cp ${debianInitScriptPath} /etc/init.d/wildfly
  sudo chown root:root /etc/init.d/wildfly
  sudo chmod +x /etc/init.d/wildfly
  sudo update-rc.d wildfly defaults
  sudo update-rc.d wildfly enable

  # Add administrator user for Wildfly
  ${jbossHome}/bin/add-user.sh ${ultimateUserName} ${ultimateUserPassword}

  # Set JVM options
  sudo tee -a cat /opt/wildfly-${jbossVersion}/bin/standalone.conf > /dev/null << EOF
JAVA_OPTS="$JAVA_OPTS -Xms256m -Xmx1024m -XX:NewSize=192m -XX:MaxNewSize=192m -verbose:gc"
EOF

  # Start Wildfly Server
  sudo service wildfly start

  # Download and deploy PostgreSQL JDBC Driver to Wildfly, configure Data Source, allow connections from host OS
  jdbcDriverVersion="9.4.1211"
  postgresqlDriverName="postgresql-${jdbcDriverVersion}.jar"
  download ${postgresqlDriverName} https://jdbc.postgresql.org/download/${postgresqlDriverName}
  configScriptName="configureWildfly.cli"
  cat > ${configScriptName} << EOF
connect
/interface=management:undefine-attribute(name=inet-address)
/interface=management:write-attribute(name=any-address,value=true)
/interface=public:undefine-attribute(name=inet-address)
/interface=public:write-attribute(name=any-address,value=true)
deploy ${DIST_DIR}/${postgresqlDriverName}
data-source add \
  --name=PostgresDS \
  --jndi-name=java:/PostgresDS \
  --driver-name=${postgresqlDriverName} \
  --connection-url=jdbc:postgresql://localhost:5432/postgres \
  --user-name=${ultimateUserName} \
  --password=${ultimateUserPassword} \
  --new-connection-sql="select 1" \
  --min-pool-size=0 \
  --max-pool-size=35 \
  --pool-prefill=true
EOF
  ${jbossHome}/bin/jboss-cli.sh --file=${configScriptName}
  rm ${configScriptName}

  # Reboot Wildfly server
  sudo service wildfly stop
  sudo service wildfly start

  echo "-----------------------------------------------"
  echo "Wildfly ${jbossVersion} installed successfully!"
  echo "-----------------------------------------------"
else
  echo "-------------------------------------------------------------------"
  echo "Wildfly ${jbossVersion} is already installed. Skipping this step..."
  echo "-------------------------------------------------------------------"
fi

# Update file system index
sudo updatedb

echo "----------------------------------------------"
echo "JavaDevVM configuration finished successfully!"
echo "----------------------------------------------"
